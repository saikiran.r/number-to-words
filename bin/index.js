#!/usr/bin/env node

const yargs = require("yargs");
const numberToWords = require("../src/numberToWords").default;

const usage = "\nUsage: number-to-words <number>";
const options = yargs  
      .usage(usage)  
      // .option("n", {alias:"number", describe: "Number between 1 to 100000 to convert into words", type: "string", demandOption
// : true })                                                                                                    
      .help(true)  
      .argv;

console.log(options["_"][0]);
const number = options["_"][0];
      
// console.log(numberToWords(options.number));
console.log(numberToWords(number));