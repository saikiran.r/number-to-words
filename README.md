# number-to-words

This is number of words nodejs cli

**To run the module**
1. Clone the repo using `git clone <clone url>`
2. Go to project folder `cd number-to-words`
3. Install the module globally `npm install -g .`
4. Run the module to use and test
eg: `number-to-words 1000`
output: one thousand

`number-to-words 1001`
output: one thousand and one
