const SINGLES_TEXT = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
      "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
      "eighteen", "nineteen", "twenty"]

const TENS_TEXT = [null, null, "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];

const THOUSANDS_TEXT = ["", "thousand"];

function numberToWords(numberStr = 0){

  const errorMessage = "Invalid Input!! Please enter an Integer Number between 0 and 100000";
  if(!isNumber(numberStr)){
    return errorMessage;
  }

  const number = parseInt(numberStr);
  if(number > 100000){
    return errorMessage;
  }

  if(number < 21){
    return SINGLES_TEXT[number];
  }
  if(number < 1000){
    return getHundredsText(number);
  }
  const groupOfThrees = convertNumberToArrayOfThrees(number);
  let result = getHundredsText(groupOfThrees[0]);
  let appendAnd = (groupOfThrees[0] > 0) && (groupOfThrees[0] < 100);
  for(let i = 1; i < groupOfThrees.length; i++){
    let prefix = `${getHundredsText(groupOfThrees[i])} ${THOUSANDS_TEXT[i]}`;
    if(groupOfThrees[0] != 0){
      prefix += (appendAnd ? " and " : ", ")
    }
    result =  prefix + result;
    appendAnd = false;
  }
  return result;

}

function getHundredsText(number){
  if(number < 1){
    return "";
  }
  if(number < 21){
    return SINGLES_TEXT[number];
  }
  if(number < 100){
    const onesPlaceDigit = number % 10;
    const tensPlaceDigit = parseInt(number/10);
    return onesPlaceDigit ? TENS_TEXT[tensPlaceDigit] +"-"+ SINGLES_TEXT[onesPlaceDigit] : TENS_TEXT[tensPlaceDigit];
  }
  if(number < 1000){
    const hundredsPlaceDigit = parseInt(number / 100);
    const tensPlaceDigits = number % 100;
    return tensPlaceDigits ? SINGLES_TEXT[hundredsPlaceDigit] + " hundred and " + getHundredsText(tensPlaceDigits)  : SINGLES_TEXT[hundredsPlaceDigit] + " hundred";
  }
  return "";
}

function isNumber(value){
  return /^\d+$/.test(value);
}

function convertNumberToArrayOfThrees(number){
  const arrayOfThrees = [];
  while(number){
    arrayOfThrees.push(number % 1000);
    number = parseInt(number/1000);
  }
  return arrayOfThrees;
}

exports.default = numberToWords; 
